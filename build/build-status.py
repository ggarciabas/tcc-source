#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['../../build/scratch/energy/ns3.18-energy-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

