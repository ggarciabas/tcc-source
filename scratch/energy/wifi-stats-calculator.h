/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2009, 2011 CTTC
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Giovanna Garcia Basilio <ggarciabas@gmail.com> (energy model)
*/


#ifndef WIFI_STATS_CALCULATOR_H
#define WIFI_STATS_CALCULATOR_H

#include <ns3/object.h>
#include <ns3/string.h>
#include <map>

namespace ns3 {

class WifiStatsCalculator : public Object
{
public:
	WifiStatsCalculator();
	virtual ~WifiStatsCalculator();
	static TypeId GetTypeId ();
	void SetOutputFilename (std::string outputFilename);
	std::string GetOutputFilename ();
private:
	std::string m_outputFilename;
};

}
#endif 
