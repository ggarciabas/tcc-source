/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Giovanna Garcia Basilio <ggarciabas@gmail.com>
 */

#include <ns3/lte-helper.h>
#include <ns3/epc-helper.h>
#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/ipv4-global-routing-helper.h>
#include <ns3/internet-module.h>
#include <ns3/mobility-module.h>
#include <ns3/lte-module.h>
#include <ns3/applications-module.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/config-store.h>
#include "lte-radio-energy-model-helper.h"
#include <ns3/ptr.h>
#include <ns3/simple-device-energy-model.h>
#include <ns3/energy-source-container.h>
#include <ns3/propagation-loss-model.h>
#include <ns3/cost231-propagation-loss-model.h>
#include <ns3/wifi-module.h>
#include "wifi-radio-energy-model-helper.h"
#include <ns3/ipv4-global-routing-helper.h>
// Basic battery
#include <ns3/basic-energy-source-helper.h>
#include <ns3/basic-energy-source.h>

#include "CApplication.h"
#include <iostream>
#include <string.h>

using namespace ns3;

Ptr<CApplication> applicationTest = CreateObject<CApplication>();;
NodeContainer mobileDevicesLTE;
NodeContainer mobileDevicesWLAN;
Ptr<LteHelper> lteHelper;
Ptr<EpcHelper> epcHelper;
NodeContainer enbNodes;
Ptr<Node> apNode;

// Devices
NetDeviceContainer ueLteDev;
NetDeviceContainer mobWlanDev;

// Energy
BasicEnergySourceHelper basicModelHelper;
Ptr<BasicEnergySource> basicSource;
	
EnergySourceContainer sourceLte1;
EnergySourceContainer sourceLte2;
EnergySourceContainer sourceWlan1;
EnergySourceContainer sourceWlan2;
DeviceEnergyModelContainer deviceModelLte1;
DeviceEnergyModelContainer deviceModelLte2;
DeviceEnergyModelContainer deviceModelWlan1;
DeviceEnergyModelContainer deviceModelWlan2;
LteRadioEnergyModelHelper radioEnergyHelperLte;
WifiRadioEnergyModelHelper radioEnergyHelperWlan;

Ptr<DeviceEnergyModel> wlanDeviceEnergy1;
Ptr<DeviceEnergyModel> wlanDeviceEnergy2;
Ptr<DeviceEnergyModel> lteDeviceEnergy1;
Ptr<DeviceEnergyModel> lteDeviceEnergy2;


void IniciandoTeste (int);

void ConfigurandoCenario (int numUes) {
  
  // Criando nós da rede
  mobileDevicesLTE.Create(numUes); 
  mobileDevicesWLAN.Create(numUes);
  
  // Criando nós para configuração da rede LTE
  lteHelper = CreateObject<LteHelper> ();
  epcHelper = CreateObject<EpcHelper> ();
  lteHelper->SetEpcHelper(epcHelper);
  //Ptr<Node> pgw = epcHelper->GetPgwNode();
  enbNodes.Create(1);
	
  // Criando nós para a configuracao da rede WLAN
  apNode = CreateObject<Node>();
  // Modelo de propagação
  Ptr<Cost231PropagationLossModel> propagation = 
    CreateObject<Cost231PropagationLossModel>();
  propagation->SetAttribute("Frequency", DoubleValue(5.753e+09));
  propagation->SetAttribute("BSAntennaHeight", DoubleValue(30));
  propagation->SetAttribute("SSAntennaHeight", DoubleValue(1));
  propagation->SetAttribute("MinDistance", DoubleValue(2000));
  // Criando o canal
  YansWifiChannelHelper channelHelper = YansWifiChannelHelper::Default ();
  Ptr<YansWifiChannel> channel = channelHelper.Create();
  channel->SetPropagationLossModel(propagation);
  // Criando camada fisica
  YansWifiPhyHelper phyHelper = YansWifiPhyHelper::Default();
  phyHelper.SetChannel (channel);
  // Configurando rede Wifi
  WifiHelper wifiHelper = WifiHelper::Default();
  wifiHelper.SetStandard(WIFI_PHY_STANDARD_80211g);
  wifiHelper.SetRemoteStationManager ("ns3::AarfWifiManager");
  // Configurando mac layer
  NqosWifiMacHelper nqosHelper = NqosWifiMacHelper::Default();
  // Configurando ssid
  Ssid ssid = Ssid ("energy");
  std::string phyMode ("DsssRate1Mbps");
  Config::SetDefault (
		      "ns3::WifiRemoteStationManager::NonUnicastMode", 
		      StringValue (phyMode));
  wifiHelper.SetRemoteStationManager (
				      "ns3::ConstantRateWifiManager", 
				      "DataMode", StringValue (phyMode), 
				      "ControlMode", StringValue(phyMode));
  nqosHelper.SetType (
		      "ns3::StaWifiMac", 
		      "Ssid", SsidValue(ssid), 
		      "ActiveProbing", BooleanValue(false));
  
  // Configuração internet
  InternetStackHelper internet;
  internet.Install(mobileDevicesLTE);	
  internet.Install(mobileDevicesWLAN);
  internet.Install(apNode);
  //internet.Install(enbNodes);
  
  // Configurando mobilidade
  MobilityHelper mobilityHelper;
  Ptr<ListPositionAllocator> enbPosition =  CreateObject<ListPositionAllocator>();
  enbPosition->Add(Vector(4,0,0));
  mobilityHelper.SetPositionAllocator(enbPosition);
  mobilityHelper.SetMobilityModel(
				  "ns3::ConstantPositionMobilityModel");
  mobilityHelper.Install(enbNodes.Get(0));
  Ptr<ListPositionAllocator> apPosition = 
    CreateObject<ListPositionAllocator>();
  apPosition->Add(Vector(-5,0,0));
  mobilityHelper.SetPositionAllocator(apPosition);
  mobilityHelper.SetMobilityModel(
				  "ns3::ConstantPositionMobilityModel");
  mobilityHelper.Install(apNode);
  mobilityHelper.SetPositionAllocator(
				      "ns3::RandomDiscPositionAllocator", 
				      "X", StringValue("-5"), 
				      "Y", StringValue("0"), 
				      "Rho", 
				      StringValue("ns3::UniformRandomVariable[Min=2|Max=25]"));
  mobilityHelper.SetMobilityModel(
				  "ns3::RandomWalk2dMobilityModel", 
				  "Mode", StringValue("Time"), 
				  "Time", StringValue("1.5s"), 
				  "Speed", 
				  StringValue("ns3::ConstantRandomVariable[Constant=1.0]"), 
				  "Bounds", RectangleValue(Rectangle(-25,25,-25,25)));
  
  for (uint16_t i =0; i< mobileDevicesLTE.GetN(); ++i){
    mobilityHelper.Install(mobileDevicesLTE.Get(i));
  }
  
  for (uint16_t i =0; i< mobileDevicesWLAN.GetN(); ++i){
    mobilityHelper.Install(mobileDevicesWLAN.Get(i));
  }
  
  // Instalando dispositivos LTE
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice(
							      enbNodes);
  ueLteDev  = lteHelper->InstallUeDevice(
					 mobileDevicesLTE);
  // Instalando dispositivos WLAN
  NetDeviceContainer apDevice;
  apDevice = wifiHelper.Install (
				 phyHelper, 
				 nqosHelper, 
				 apNode);
  mobWlanDev = wifiHelper.Install (
				   phyHelper, 
				   nqosHelper, 
				   mobileDevicesWLAN);
  
  // Associando IPs para os dispositivos dos 'celulares'
  Ipv4InterfaceContainer ueIpInterface;
  ueIpInterface = epcHelper->AssignUeIpv4Address(
						 NetDeviceContainer(ueLteDev));	
  Ipv4AddressHelper address;
  address.SetBase ("11.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer apIpInterface;
  apIpInterface = address.Assign (apDevice);
  Ipv4InterfaceContainer mobIpInterface;
  mobIpInterface = address.Assign (mobWlanDev);
  // Configurando roteamento
  Ipv4StaticRoutingHelper routing;
  for (uint32_t u = 0; u < mobileDevicesLTE.GetN(); ++u) {
    Ptr<Node> ueNode = mobileDevicesLTE.Get(u);
    Ptr<Ipv4StaticRouting> ueStaticRouting = 
      routing.GetStaticRouting(
			       ueNode->GetObject<Ipv4> ());
    ueStaticRouting->SetDefaultRoute(
				     epcHelper->GetUeDefaultGatewayAddress(), 1);
  } 
  
  for (uint32_t u = 0; u < mobileDevicesWLAN.GetN(); ++u) {
    Ptr<Node> ueNode = mobileDevicesWLAN.Get(u);
    Ptr<Ipv4StaticRouting> apRouting = 
      routing.GetStaticRouting(
			       ueNode->GetObject<Ipv4>());
    apRouting->SetDefaultRoute(Ipv4Address("11.0.0.1"), 1); 
  } 
  
  lteHelper->Attach ( ueLteDev, enbLteDevs.Get(0) );          
}

void ConfigurandoModeloEnergetico (int teste) {

  // Instalando bateria (energySource) 
  basicModelHelper.Set("BasicEnergySourceInitialEnergyJ", DoubleValue(5)); // 5J ( joules)
  basicModelHelper.Set("BasicEnergySupplyVoltageV", DoubleValue(3.7)); // 3.7V (volts)

  if (teste % 2 == 0) 
    { // WLAN
      // Configurando os valores de idle, tx e rx para WLAN
      radioEnergyHelperWlan.Set("IdleCurrentA", DoubleValue(0.0012));
      radioEnergyHelperWlan.Set("RxCurrentA", DoubleValue(0.1));
      radioEnergyHelperWlan.Set("TxCurrentA", DoubleValue(0.185));

      sourceWlan1 = basicModelHelper.Install(mobileDevicesWLAN.Get(0));
      sourceWlan2 = basicModelHelper.Install(mobileDevicesWLAN.Get(1));
      // Instalando DeviceEnergyModel WLAN
      deviceModelWlan1 = radioEnergyHelperWlan.Install(
						       mobWlanDev.Get(0), 
						       sourceWlan1);
      deviceModelWlan2 = radioEnergyHelperWlan.Install(
						       mobWlanDev.Get(1), 
						       sourceWlan2); 
    } 
  else {	    // LTE
    // Configurando os valores de idle, tx e rx para LTE
    radioEnergyHelperLte.Set("IdleCurrentA", DoubleValue(0.0012));
    radioEnergyHelperLte.Set("RxCtrlCurrentA", DoubleValue(0.092));
    radioEnergyHelperLte.Set("RxDataCurrentA", DoubleValue(0.092));
    radioEnergyHelperLte.Set("TxCurrentA", DoubleValue(0.148));

    sourceLte1 = basicModelHelper.Install(mobileDevicesLTE.Get(0));
    sourceLte2 = basicModelHelper.Install(mobileDevicesLTE.Get(1)); 
    // Instalando DeviceEnergyModel LTE
    deviceModelLte1 = radioEnergyHelperLte.Install(
						   ueLteDev.Get(0), 
						   sourceLte1);        
    deviceModelLte2 = radioEnergyHelperLte.Install(
						   ueLteDev.Get(1), 
						   sourceLte2);    
  }	  	
}

int main(int argc, char *argv[]) {	
  LogComponentEnable ("WifiRadioEnergyStatsCalculator", LOG_LEVEL_INFO);
  LogComponentEnable ("LteRadioEnergyModelHelper", LOG_LEVEL_INFO);
  uint16_t numeroNos = 2;
  int teste = 1;
  
  // Verificando o número do teste informado por linha de comando
  CommandLine cmd;
  cmd.AddValue("teste", "Teste a ser realizado.", teste);
  cmd.Parse(argc, argv);		
  
  // Configurando o cenário
  ConfigurandoCenario(numeroNos);
  
  // Iniando XML
  uint32_t sender = (teste%2 == 0) ? /*Wlan*/ mobileDevicesWLAN.Get(1)->GetId() : /*LTE*/  mobileDevicesLTE.Get(1)->GetId();
  uint32_t receiver = (teste%2 == 0) ? /*Wlan*/ mobileDevicesWLAN.Get(0)->GetId() : /*LTE*/  mobileDevicesLTE.Get(0)->GetId();
  NS_LOG_UNCOND ("<Log>\n\t<Teste>" << teste << "</Teste>\n\t<Sender>" 
		 << sender << "</Sender>\n\t<Receiver>" 
		 << receiver << "</Receiver>\n\t<Resultado>");
  
  // configurando modelo energético
  ConfigurandoModeloEnergetico(teste);		
  
  // Iniciando teste
  IniciandoTeste (teste);
  
  switch (teste) {
  case 1:
  case 2:
    Simulator::Stop(Seconds(11.));
    break;
  case 3:    
  case 4:
    Simulator::Stop(Seconds(21.));
    break;
  case 5:
  case 6:
    Simulator::Stop(Seconds(31.));
    break;
  }

  Simulator::Run();
  Simulator::Destroy();
  
  NS_LOG_UNCOND("\t</Resultado>\n</Log>");
  
  return 0;
}

void IniciandoTeste (int teste) {
  int port;
  Address address;
  Ptr<Socket> socket;
  int bits;
  std::string rate;
  int size;
  ApplicationContainer appContainer;
  
  switch (teste) {
  case 1:{ 
    port = 5060;
    bits = 24 * 1024;
    rate = "0.024Mbps";
    size = 50;
    // Quem recebera o pacote
    address = InetSocketAddress (	
				 mobileDevicesLTE.Get(0)
				 ->GetObject<Ipv4>()
				 ->GetAddress(1,0)
				 .GetLocal(), 
				 port);
    PacketSinkHelper packetSinkHelper (
				       "ns3::UdpSocketFactory", 
				       InetSocketAddress(Ipv4Address::GetAny(), port));
    appContainer = packetSinkHelper.Install(mobileDevicesLTE.Get(0));
    // Quem enviara o pacote
    socket = Socket::CreateSocket(
				  mobileDevicesLTE.Get(1), 
				  UdpSocketFactory::GetTypeId());
    // Configurando a aplicação no dispositivo
    mobileDevicesLTE.Get(1)->AddApplication(applicationTest);

    applicationTest->Setup (socket, address, size, 10000, rate);
  
    appContainer.Start(Seconds(0.));
    appContainer.Stop(Seconds(10.));
  
    applicationTest->SetStartTime(Seconds(1.));
    applicationTest->SetStopTime(Seconds(10.));

  }
    break;
  case 2:{ 
    port = 5060;
    bits = 24 * 1024;
    rate = "0.024Mbps";
    size = 50;
    // Quem recebera o pacote
    address = InetSocketAddress (
				 mobileDevicesWLAN.Get(0)
				 ->GetObject<Ipv4>()
				 ->GetAddress(1,0)
				 .GetLocal(), 
				 port);
    PacketSinkHelper packetSinkHelper (
				       "ns3::UdpSocketFactory", 
				       InetSocketAddress(Ipv4Address::GetAny(), port));
    appContainer = packetSinkHelper.Install(mobileDevicesWLAN.Get(0));
    // Quem enviara o pacote
    socket = Socket::CreateSocket(
				  mobileDevicesWLAN.Get(1), 
				  UdpSocketFactory::GetTypeId());
    // Configurando a aplicacao no dispositivo
    mobileDevicesWLAN.Get(1)->AddApplication(applicationTest);

    applicationTest->Setup (socket, address, size, 10000, rate);
  
    appContainer.Start(Seconds(0.));
    appContainer.Stop(Seconds(10.));
  
    applicationTest->SetStartTime(Seconds(1.));
    applicationTest->SetStopTime(Seconds(10.));

  }
    break;
  case 3:{ 
    port = 5060;
    bits = 24 * 1024;
    rate = "0.024Mbps";
    size = 50;
    // Quem recebera o pacote
    address = InetSocketAddress (	
				 mobileDevicesLTE.Get(0)
				 ->GetObject<Ipv4>()
				 ->GetAddress(1,0)
				 .GetLocal(), 
				 port);
    PacketSinkHelper packetSinkHelper (
				       "ns3::UdpSocketFactory", 
				       InetSocketAddress(Ipv4Address::GetAny(), port));
    appContainer = packetSinkHelper.Install(mobileDevicesLTE.Get(0));
    // Quem enviara o pacote
    socket = Socket::CreateSocket(
				  mobileDevicesLTE.Get(1), 
				  UdpSocketFactory::GetTypeId());
    // Configurando a aplicação no dispositivo
    mobileDevicesLTE.Get(1)->AddApplication(applicationTest);

    applicationTest->Setup (socket, address, size, 10000, rate);
  
    appContainer.Start(Seconds(0.));
    appContainer.Stop(Seconds(20.));
  
    applicationTest->SetStartTime(Seconds(1.));
    applicationTest->SetStopTime(Seconds(20.));

  }
    break;
  case 4:{ 
    port = 5060;
    bits = 24 * 1024;
    rate = "0.024Mbps";
    size = 50;
    // Quem recebera o pacote
    address = InetSocketAddress (
				 mobileDevicesWLAN.Get(0)
				 ->GetObject<Ipv4>()
				 ->GetAddress(1,0)
				 .GetLocal(), 
				 port);
    PacketSinkHelper packetSinkHelper (
				       "ns3::UdpSocketFactory", 
				       InetSocketAddress(Ipv4Address::GetAny(), port));
    appContainer = packetSinkHelper.Install(mobileDevicesWLAN.Get(0));
    // Quem enviara o pacote
    socket = Socket::CreateSocket(
				  mobileDevicesWLAN.Get(1), 
				  UdpSocketFactory::GetTypeId());
    // Configurando a aplicacao no dispositivo
    mobileDevicesWLAN.Get(1)->AddApplication(applicationTest);

    applicationTest->Setup (socket, address, size, 10000, rate);
  
    appContainer.Start(Seconds(0.));
    appContainer.Stop(Seconds(20.));
  
    applicationTest->SetStartTime(Seconds(1.));
    applicationTest->SetStopTime(Seconds(20.));

  }
    break;
  case 5:{ 
    port = 5060;
    bits = 24 * 1024;
    rate = "0.024Mbps";
    size = 50;
    // Quem recebera o pacote
    address = InetSocketAddress (	
				 mobileDevicesLTE.Get(0)
				 ->GetObject<Ipv4>()
				 ->GetAddress(1,0)
				 .GetLocal(), 
				 port);
    PacketSinkHelper packetSinkHelper (
				       "ns3::UdpSocketFactory", 
				       InetSocketAddress(Ipv4Address::GetAny(), port));
    appContainer = packetSinkHelper.Install(mobileDevicesLTE.Get(0));
    // Quem enviara o pacote
    socket = Socket::CreateSocket(
				  mobileDevicesLTE.Get(1), 
				  UdpSocketFactory::GetTypeId());
    // Configurando a aplicação no dispositivo
    mobileDevicesLTE.Get(1)->AddApplication(applicationTest);

    applicationTest->Setup (socket, address, size, 10000, rate);
  
    appContainer.Start(Seconds(0.));
    appContainer.Stop(Seconds(30.));
  
    applicationTest->SetStartTime(Seconds(1.));
    applicationTest->SetStopTime(Seconds(30.));

  }
    break;
  case 6:{ 
    port = 5060;
    bits = 24 * 1024;
    rate = "0.024Mbps";
    size = 50;
    // Quem recebera o pacote
    address = InetSocketAddress (
				 mobileDevicesWLAN.Get(0)
				 ->GetObject<Ipv4>()
				 ->GetAddress(1,0)
				 .GetLocal(), 
				 port);
    PacketSinkHelper packetSinkHelper (
				       "ns3::UdpSocketFactory", 
				       InetSocketAddress(Ipv4Address::GetAny(), port));
    appContainer = packetSinkHelper.Install(mobileDevicesWLAN.Get(0));
    // Quem enviara o pacote
    socket = Socket::CreateSocket(
				  mobileDevicesWLAN.Get(1), 
				  UdpSocketFactory::GetTypeId());
    // Configurando a aplicacao no dispositivo
    mobileDevicesWLAN.Get(1)->AddApplication(applicationTest);

    applicationTest->Setup (socket, address, size, 10000, rate);
  
    appContainer.Start(Seconds(0.));
    appContainer.Stop(Seconds(30.));
  
    applicationTest->SetStartTime(Seconds(1.));
    applicationTest->SetStopTime(Seconds(30.));

  }
    break;
  }
  

  
}			
