#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# 
# Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
#

principal()
{
        clear
#        echo -n " Informe o teste: "
#	read i
	for i in {15..18}
	do
		case $i in
			1) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			2) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			3) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			4) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			5) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			6) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			7) simula 1 $i
			   simula 3 $i
			   simula 5 $i;; 
			8) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			9) simula 2 $i
			   simula 4 $i
			   simula 6 $i;;
			10) simula 2 $i
			   simula 4 $i
			   simula 6 $i;;
			11)simula 2 $i
			   simula 4 $i
			   simula 6 $i;;
			12) simula 2 $i
			   simula 4 $i
			   simula 6 $i;;
			13) simula 2 $i
			   simula 4 $i
			   simula 6 $i;;
			15) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			16) simula 1 $i
			   simula 3 $i
			   simula 5 $i;;
			17) simula 2 $i
			   simula 4 $i
			   simula 6 $i;;
			18) simula 2 $i
			   simula 4 $i
			   simula 6 $i;;
		esac
	done
}

simula()
{
	DATA=`date +%D`
        TEMPO=`date +%r`
	echo -n "Recuperando resultados do teste ($2 -- $1) - $DATA $TEMPO ..."
	./resultados $1 $2
}

principal;

