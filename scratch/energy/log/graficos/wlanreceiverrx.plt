set terminal png
set output 'wlanreceiverrx.png'
set title 'Consumo energético operação RX receiver - WLAN'

#set logscale xy
unset log y2

set xlabel 'Teste'
set xrange [0:4]
set ylabel 'Tempo (s)'
set yrange [0:31]
set y2label 'Consumo energético por segundo (J)'
set y2range [0:0.2]
set ytics nomirror
set y2tics
set tics out

set boxwidth 0.5
set style fill solid

set style line 1 lc rgb "orange"
set style line 2 lc rgb 'black' lt 1 lw 2 pt 13 ps 1.5   # --- red

plot "time" using 1:3:xtic(2) axis x1y1 with boxes ls 1 notitle,\
"wlanreceiverrx" using 1:3:xtic(2) axis x1y2 with linespoints ls 2 notitle

