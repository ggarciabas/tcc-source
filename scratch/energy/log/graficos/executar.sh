#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# 
# Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
#

principal () {
	clear
	DATA=`date +%D`
        TEMPO=`date +%r`
	echo -n "Iniciando a geração dos gráficos - $DATA $TEMPO... "
#	gnuplot wlanreceiverrx.plt
#	gnuplot ltereceiverrxdata.plt
#	gnuplot ltesendertx.plt
#	gnuplot wlansendertx.plt	
#	gnuplot ltesendertotal.plt
#	gnuplot ltereceivertotal.plt
#	gnuplot wlanreceivertotal.plt
#	gnuplot wlansendertotal.plt
#	gnuplot ltesenderrxdata.plt
#	gnuplot ltereceivertx.plt
#	gnuplot ltereceiverrxctrl.plt
#	gnuplot ltesenderrxctrl.plt
	gnuplot ltereceiveridle.plt
	gnuplot ltesenderidle.plt
	echo -n "Gráficos gerados!"
}

principal
