/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 *
 * Obs.: g++ -o lte lte.cc $(xml2-config --cflags --libs)
 */

#include <libxml/parser.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <sstream>

#define CMP(arg1, arg2) xmlStrcmp (arg1, (const xmlChar *) arg2)

class Lte 
{
private:
  double time;
  double start;
  double duration;
  int imsi;
  std::string link;
  std::string state;
  double energyDecrease;
  double totalEnergy;
  double txPSD;
  double nRB;

public:
  Lte() {}
  ~Lte() {}
  void setTime (double t) {time = t;}
  void setStart (double s) {start = s;}
  void setDuration (double d) { duration = d;}
  void setImsi (int i) {imsi = i;}
  void setLink (std::string s) {link = s;}
  void setState (std::string s) {state = s;}
  void setEnergyDecrease (double e) {energyDecrease = e;}
  void setTotalEnergy (double t) {totalEnergy = t;}
  void setTxPSD (double t) {txPSD = t;}
  void setnRB (double n) {nRB = n;}
  double getTime (){return time;}
  double getStart () {return start;}
  int getImsi () {return imsi;}
  std::string getLink () {return link;}
  std::string getState () {return state;}
  double getEnergyDecerease () {return energyDecrease;}
  double getTotalEnergy () {return totalEnergy;}
  double getTxPSD () {return txPSD;}
  double getnRB () {return nRB;}
};

class Resultado 
{
private:
  std::vector<Lte*> lte;
public:
  Resultado() {}
  ~Resultado() {}
  void addLte (Lte* l) {
    lte.push_back(l);
  }
  Lte* getPostLte (int p) {
    return lte.at(p);
  }
  int getN () {
    return lte.size();
  }
};

class Log 
{
private: 
  int teste;
  Resultado* resultado;

public:
  Log() {}
  ~Log() {}
  void setTeste (int t) {
    teste = t;
  }
  void setResultado (Resultado* r) {
    resultado = r;
  }
  int getTeste () {
    return teste;
  }
  Resultado* getResultado () {
    return resultado;
  }
};

Lte* parseLte (xmlDocPtr doc, xmlNodePtr node) 
{
  Lte* lte = new Lte ();
  xmlChar *key;
  for (node = node->xmlChildrenNode; node != NULL; node = node->next)
    {
      key = xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
      if (! CMP(node->name, "time")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setTime(d);
	std::cout << "\t\tTime: " << d << std::endl;
      } else if (! CMP (node->name, "start")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setStart (d);
	std::cout << "\t\tStart: " << d << std::endl;
      } else if (! CMP (node->name, "duration")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setDuration(d);
	std::cout << "\t\tDuration: " << d << std::endl;
      } else if (! CMP (node->name, "imsi")) {
	std::istringstream iss ((char *) key);
	int i;
	iss >> i;
	lte->setImsi (i);
	std::cout << "\t\tImsi: " << i << std::endl;
      } else if (! CMP (node->name, "link")) {
	lte->setLink ((char *) key);
	std::cout << "\t\tLink: " << key << std::endl;
      } else if (! CMP (node->name, "state")) {
	lte->setState ((char *) key);
	std::cout << "\t\tLink: " << key << std::endl;
      } else if (! CMP (node->name, "energyDecrease")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setEnergyDecrease (d);
	std::cout << "\t\tEnergyDecrease: " << d << std::endl;
      } else if (! CMP (node->name, "totalEnergy")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setTotalEnergy (d);
	std::cout << "\t\tTotalEnergy: " << d << std::endl;
      } else if (! CMP (node->name, "txPSD")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setTxPSD (d);
	std::cout << "\t\tTxPSD: " << d << std::endl;
      } else if (! CMP (node->name, "nRB")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setnRB (d);  
	std::cout << "\t\tnRB: " << d << std::endl;    
      }
    }
  return lte;
}

Resultado* parseResultado (xmlDocPtr doc, xmlNodePtr node) 
{
  Resultado* res = new Resultado();
  
  for (node = node->xmlChildrenNode; node != NULL; node = node->next) 
    {
      //std::cout << "Node name: " << node->name << std::endl;
      if (! CMP (node->name, "lte")) 
	{
	  Lte* lte;
	  lte = parseLte (doc, node);
	  res->addLte (lte);
	  std::cout << "\tLte inserido" << std::endl;	 
	}
    }

  return res;
}

Log* parseLog (xmlDocPtr doc, xmlNodePtr node) 
{
  Log* log = new Log();
  xmlChar *key;

  for (node = node->xmlChildrenNode; node != NULL; node = node->next) 
    {
      //std::cout << "Node name: " << node->name << std::endl;
      key = xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
      //std::cout << "leu key" << std::endl;
      if (! CMP (node->name, "teste")) {
	std::istringstream iss ((char *) key);
	int i;
	iss >> i;
	log->setTeste (i);
	std::cout << "Teste: " << i << std::endl;
      } else if (! CMP (node->name, "resultado")) {
	Resultado* res;
	res = parseResultado (doc, node);
	log->setResultado (res);
	//std::cout << "Resultado inserido" << std::endl;
      }
      xmlFree ( key );
    }
  return log;
}

int main () {
  xmlDocPtr doc;
  xmlNodePtr node;

  doc = xmlParseFile ("log1.xml");
  std::cout << "Leu xml" << std::endl;
  node = xmlDocGetRootElement (doc);
  std::cout << "Pegou o elemento root" << std::endl;

  Log* log;

  std::cout << "Iniciando parse Log" << std::endl;
  log = parseLog (doc, node);

  xmlFreeDoc (doc);

  // Trabalhar com o log

  	

  return 0;
}
