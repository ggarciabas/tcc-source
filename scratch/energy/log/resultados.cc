/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 *
 * Obs.: g++ -o resultado resultado.cc $(xml2-config --cflags --libs)
 */

#include <libxml/parser.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>

#define CMP(arg1, arg2) xmlStrcmp (arg1, (const xmlChar *) arg2)

bool isWlan;

class Wlan 
{
private:
  double time;
  double start;
  double duration;
  int node;
  std::string state;
  double energyDecrease;
  double totalEnergy;

public:
  Wlan() {}
  ~Wlan() {}
  void setTime (double t) {time = t;}
  void setStart (double s) {start = s;}
  void setDuration (double d) { duration = d;}
  void setNode (int i) {node = i;}
  void setState (std::string s) {state = s;}
  void setEnergyDecrease (double e) {energyDecrease = e;}
  void setTotalEnergy (double t) {totalEnergy = t;}
  double getTime (){return time;}
  double getStart () {return start;}
  int getNode () {return node;}
  std::string getState () {return state;}
  double getEnergyDecerease () {return energyDecrease;}
  double getTotalEnergy () {return totalEnergy;}
};

class Lte 
{
private:
  double time;
  double start;
  double duration;
  int node;
  int imsi;
  std::string link;
  std::string state;
  double energyDecrease;
  double totalEnergy;
  double txPSD;
  double nRB;

public:
  Lte() {}
  ~Lte() {}
  void setTime (double t) {time = t;}
  void setStart (double s) {start = s;}
  void setDuration (double d) { duration = d;}
  void setImsi (int i) {imsi = i;}
  void setNode (int n) {node = n;}
  void setLink (std::string s) {link = s;}
  void setState (std::string s) {state = s;}
  void setEnergyDecrease (double e) {energyDecrease = e;}
  void setTotalEnergy (double t) {totalEnergy = t;}
  void setTxPSD (double t) {txPSD = t;}
  void setnRB (double n) {nRB = n;}
  double getTime (){return time;}
  double getStart () {return start;}
  int getImsi () {return imsi;}
  int getNode () {return node;}
  std::string getLink () {return link;}
  std::string getState () {return state;}
  double getEnergyDecerease () {return energyDecrease;}
  double getTotalEnergy () {return totalEnergy;}
  double getTxPSD () {return txPSD;}
  double getnRB () {return nRB;}
};

class Resultado 
{
private:
  std::vector<Lte*> lte;
  std::vector<Wlan*> wlan;	
public:
  Resultado() {}
  ~Resultado() {}
  void addLte (Lte* l) {
    lte.push_back(l);
  }
  Lte* getPosLte (int p) {
    return lte.at(p);
  }
  int getNLte () {
    return lte.size();
  }
  void addWlan (Wlan* w) {
    wlan.push_back(w);
  }
  Wlan* getPosWlan (int p) {
    return wlan.at(p);
  }
  int getNWlan () {
    return wlan.size();
  }
};

class Log 
{
private: 
  int teste;
  int sender;
  int receiver;
  Resultado* resultado;

public:
  Log() {}
  ~Log() {}
  void setTeste (int t) {
    teste = t;
  }
  void setSender (int t) {
    sender = t;
  }
  void setReceiver (int t) {
    receiver = t;
  }
  void setResultado (Resultado* r) {
    resultado = r;
  }
  int getTeste () {
    return teste;
  }
  int getSender () {
    return sender;
  }
  int getReceiver () {
    return receiver;
  }
  Resultado* getResultado () {
    return resultado;
  }
};

Wlan* parseWlan (xmlDocPtr doc, xmlNodePtr node) 
{
  Wlan* wlan = new Wlan ();
  xmlChar *key;
  for (node = node->xmlChildrenNode; node != NULL; node = node->next)
    {
      key = xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
      if (! CMP(node->name, "time")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	wlan->setTime(d);
	//std::cout << "\t\tTime: " << d << std::endl;
      } else if (! CMP (node->name, "start")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	wlan->setStart (d);
	//std::cout << "\t\tStart: " << d << std::endl;
      } else if (! CMP (node->name, "duration")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	wlan->setDuration(d);
	//std::cout << "\t\tDuration: " << d << std::endl;
      } else if (! CMP (node->name, "node")) {
	std::istringstream iss ((char *) key);
	int n;
	iss >> n;
	wlan->setNode (n);
	//std::cout << "\t\tNode: " << n << std::endl;
      } else if (! CMP (node->name, "state")) {
	wlan->setState ((char *) key);
	//std::cout << "\t\tState: " << key << std::endl;
      } else if (! CMP (node->name, "energyDecrease")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	wlan->setEnergyDecrease (d);
	//std::cout << "\t\tEnergyDecrease: " << d << std::endl;
      } else if (! CMP (node->name, "totalEnergy")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	wlan->setTotalEnergy (d);
	//std::cout << "\t\tTotalEnergy: " << d << std::endl;
      } 
    }
  return wlan;
}

Lte* parseLte (xmlDocPtr doc, xmlNodePtr node) 
{
  Lte* lte = new Lte ();
  xmlChar *key;
  for (node = node->xmlChildrenNode; node != NULL; node = node->next)
    {
      key = xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
      if (! CMP(node->name, "time")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setTime(d);
	//std::cout << "\t\tTime: " << d << std::endl;
      } else if (! CMP (node->name, "start")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setStart (d);
	//std::cout << "\t\tStart: " << d << std::endl;
      } else if (! CMP (node->name, "duration")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setDuration(d);
	//std::cout << "\t\tDuration: " << d << std::endl;
      } else if (! CMP (node->name, "node")) {
	std::istringstream iss ((char *) key);
	int n;
	iss >> n;
	lte->setNode (n);
	//std::cout << "\t\tNode: " << n << std::endl;
      } else if (! CMP (node->name, "imsi")) {
	std::istringstream iss ((char *) key);
	int i;
	iss >> i;
	lte->setImsi (i);
	//std::cout << "\t\tImsi: " << i << std::endl;
      } else if (! CMP (node->name, "link")) {
	lte->setLink ((char *) key);
	//std::cout << "\t\tLink: " << key << std::endl;
      } else if (! CMP (node->name, "state")) {
	lte->setState ((char *) key);
	//std::cout << "\t\tState: " << key << std::endl;
      } else if (! CMP (node->name, "energyDecrease")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setEnergyDecrease (d);
	//std::cout << "\t\tEnergyDecrease: " << d << std::endl;
      } else if (! CMP (node->name, "totalEnergy")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setTotalEnergy (d);
	//std::cout << "\t\tTotalEnergy: " << d << std::endl;
      } else if (! CMP (node->name, "txPSD")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setTxPSD (d);
	//std::cout << "\t\tTxPSD: " << d << std::endl;
      } else if (! CMP (node->name, "nRB")) {
	std::istringstream iss ((char *) key);
	double d;
	iss >> d;
	lte->setnRB (d);  
	//std::cout << "\t\tnRB: " << d << std::endl;    
      }
    }
  return lte;
}

Resultado* parseResultado (xmlDocPtr doc, xmlNodePtr node) 
{
  Resultado* res = new Resultado();
  
  for (node = node->xmlChildrenNode; node != NULL; node = node->next) 
    {
      //std::cout << "Node name: " << node->name << std::endl;
      if (! CMP (node->name, "Lte")) 
	{
	  Lte* lte;
	  lte = parseLte (doc, node);
	  res->addLte (lte);
	  //std::cout << "\tLte inserido" << std::endl;	 
	} else if (! CMP (node->name, "Wlan")) 
		{
		   Wlan* wlan;
		   wlan = parseWlan (doc, node);
		   res->addWlan (wlan);
		   //std::cout << "\tWlan inserido" << std::endl;	
		}
    }

  return res;
}

Log* parseLog (xmlDocPtr doc, xmlNodePtr node) 
{
  Log* log = new Log();
  xmlChar *key;

  for (node = node->xmlChildrenNode; node != NULL; node = node->next) 
    {
      //std::cout << "Node name: " << node->name << std::endl;
      key = xmlNodeListGetString (doc, node->xmlChildrenNode, 1);
      //std::cout << "leu key" << std::endl;
      if (! CMP (node->name, "Teste")) {
	std::istringstream iss ((char *) key);
	int i;
	iss >> i;
	log->setTeste (i);
	//std::cout << "Teste: " << i << std::endl;
      } else if (! CMP (node->name, "Sender")) {
	std::istringstream iss ((char *) key);
	int i;
	iss >> i;
	log->setSender (i);
	//std::cout << "Sender: " << i << std::endl;
      } else if (! CMP (node->name, "Receiver")) {
	std::istringstream iss ((char *) key);
	int i;
	iss >> i;
	log->setReceiver (i);
	//std::cout << "Receiver: " << i << std::endl;
      } else if (! CMP (node->name, "Resultado")) {
	Resultado* res;
	res = parseResultado (doc, node);
	log->setResultado (res);
	//std::cout << "Resultado inserido" << std::endl;
      }
      xmlFree ( key );
    }
  return log;
}

int main (int argc, char* argv[]) {
  xmlDocPtr doc;
  xmlNodePtr node;

  // Verificando se o log é wlan ou lte
  int teste = atoi(argv[1]);
  int grafico = atoi(argv[2]);
  isWlan = (teste % 2 == 0) ? true : false;

  std::ostringstream arquivo;
  arquivo << "teste-" << teste << "/teste-" << teste << ".xml";
  std::string arq;
  arq = arquivo.str();
  //std::cout << arq.c_str() << std::endl;
  doc = xmlParseFile (arq.c_str());
  //std::cout << "Leu xml" << std::endl;
  node = xmlDocGetRootElement (doc);
  //std::cout << "Pegou o elemento root" << std::endl;

  Log* log; 

  //std::cout << "Iniciando parse Log" << std::endl;
  log = parseLog (doc, node);

  xmlFreeDoc (doc);

  // Trabalhar com o log
  double energy = 0.0;
  for (int c=0; c < ((isWlan) ? log->getResultado()->getNWlan() : log->getResultado()->getNLte()); ++c) {
	switch (grafico) {
	case 1: // ltereceiverrxctrl    
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getReceiver () && log->getResultado()->getPosLte(c)->getState() == "RX_CTRL") {
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      }
	break;
	case 2: // ltereceiverrxdata   
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getReceiver () && log->getResultado()->getPosLte(c)->getState() == "RX_DATA") {
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      } 
	break;
	case 3: // ltereceivertotal
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getReceiver ()){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      } 
	break;
	case 4: // ltereceivertx
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getReceiver () && log->getResultado()->getPosLte(c)->getState() == "TX"){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      } 
	break;
	case 5: // ltesenderrxctrl
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getSender () && log->getResultado()->getPosLte(c)->getState() == "RX_CTRL"){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      }		   
	break;
	case 6: // ltesenderrxdata
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getSender () && log->getResultado()->getPosLte(c)->getState() == "RX_DATA"){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      }	
	break;
	case 7: // ltesendertotal
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getSender ()){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      }	
	break;
	case 8: // ltesendertx
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getSender () && log->getResultado()->getPosLte(c)->getState() == "TX"){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      }	
	break;
	case 9: // wlanreceiverrx
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getReceiver () && log->getResultado()->getPosWlan(c)->getState() == "RX"){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }	
	break;
	case 10: // wlanreceivertotal
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getReceiver ()){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }	
	break;
	case 11: // wlanreceivertx
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getReceiver () && log->getResultado()->getPosWlan(c)->getState() == "TX"){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }	
	break;
	case 12: // wlansendertotal
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getSender ()){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }	
	break;
	case 13: // wlansendertx
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getSender () && log->getResultado()->getPosWlan(c)->getState() == "TX"){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }
	break;
	case 14: // wlansenderrx
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getSender () && log->getResultado()->getPosWlan(c)->getState() == "RX"){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }
	break;
	case 15: // ltesenderidle
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getSender () && log->getResultado()->getPosLte(c)->getState() == "IDLE"){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      }
	break;
	case 16: // ltereceiveridle
	      if (log->getResultado()->getPosLte(c)->getNode() == log->getReceiver () && log->getResultado()->getPosLte(c)->getState() == "IDLE"){
		energy += log->getResultado()->getPosLte(c)->getEnergyDecerease();
	      }
	break;
	case 17: // wlansenderidle
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getSender () && log->getResultado()->getPosWlan(c)->getState() == "IDLE"){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }
	break;
	case 18: // wlanreceiveridle
	      if (log->getResultado()->getPosWlan(c)->getNode() == log->getReceiver () && log->getResultado()->getPosWlan(c)->getState() == "IDLE"){
		energy += log->getResultado()->getPosWlan(c)->getEnergyDecerease();
	      }
	break;
 	}
  }  	

  switch (grafico) {
	case 1: // ltereceiverrxctrl    
	      std::cout << std::endl << "ltereceiverrxctrl: " << teste << " energy: " << energy << std::endl;
	break;
	case 2: // ltereceiverrxdata   
	      std::cout << std::endl<< "ltereceiverrxdata: " << teste << " energy: " << energy << std::endl; 
	break;
	case 3: // ltereceivertotal
	      std::cout << std::endl<< "ltereceivertotal: " << teste << " energy: " << energy << std::endl; 
	break;
	case 4: // ltereceivertx
	      std::cout << "ltereceivertx: " << teste << " energy: " << energy << std::endl; 
	break;
	case 5: // ltesenderrxctrl
	      std::cout << std::endl<< "ltesenderrxctrl: " << teste << " energy: " << energy << std::endl;		   
	break;
	case 6: // ltesenderrxdata
	      std::cout << std::endl<< "ltesenderrxdata: " << teste << " energy: " << energy << std::endl;	
	break;
	case 7: // ltesendertotal
	      std::cout << std::endl<< "ltesendertotal: " << teste << " energy: " << energy << std::endl;
	break;
	case 8: // ltesendertx
	     std::cout << std::endl<< "ltesendertx: " << teste << " energy: " << energy << std::endl;
	break;
	case 9: // wlanreceiverrx
	      std::cout << std::endl<< "wlanreceiverrx: " << teste << " energy: " << energy << std::endl;	
	break;
	case 10: // wlanreceivertotal
	      std::cout << std::endl<< "wlanreceivertotal: " << teste << " energy: " << energy << std::endl;	
	break;
	case 11: // wlanreceivertx
	      std::cout << std::endl<< "wlanreceivertx: " << teste << " energy: " << energy << std::endl;	
	break;
	case 12: // wlansendertotal
	     std::cout << std::endl<< "wlansendertotal: " << teste << " energy: " << energy << std::endl;
	break;
	case 13: // wlansendertx
	      std::cout << std::endl<< "wlansendertx: " << teste << " energy: " << energy << std::endl;
	break;
	case 14: // wlansenderrx
	     std::cout << std::endl<< "wlansenderrx: " << teste << " energy: " << energy << std::endl;
	break;
	case 15: // ltesenderidle
	      std::cout << std::endl<< "ltesenderidle: " << teste << " energy: " << energy << std::endl;
	break;
	case 16: // ltereceiveridle
	     std::cout << std::endl<< "ltereceiveridle: " << teste << " energy: " << energy << std::endl;
	break;
	case 17: // wlansenderidle
	      std::cout << std::endl<< "wlansenderidle: " << teste << " energy: " << energy << std::endl;
	break;
	case 18: // wlanreceiveridle
	     std::cout << std::endl<< "wlanreceiveridle: " << teste << " energy: " << energy << std::endl;
	break;
	}

 // sender.close();
//  receiver.close();

  return 0;
}
