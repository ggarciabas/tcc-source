﻿/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#ifndef CAPPLICATION_H
#define CAPPLICATION_H

#include <ns3/applications-module.h>
#include <ns3/ptr.h>
#include <ns3/core-module.h>

using namespace ns3;
using namespace std;


class CApplication : public Application
{
private:
  Ptr<Socket>     m_socket;
  Address         m_peer;
  uint32_t        m_packetSize;
  uint32_t        m_nPackets;
  DataRate        m_dataRate;
  EventId         m_sendEvent;
  bool            m_running;
  uint32_t        m_packetsSent;

  virtual void
  StartApplication (void);
  virtual void
  StopApplication (void);

  void
  ScheduleTx (void);
  void
  SendPacket (void);

  void
    NotifyUeHandover ();

public:
  CApplication ();
  virtual
  ~CApplication ();

  void 
	Setup (Ptr<Socket> socket, Address address, uint32_t packetSize,  uint32_t nPackets, DataRate dataRate);
};

#endif // CAPPLICATION_H


