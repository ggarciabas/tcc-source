/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Giovanna Garcia Basilio <ggarciabas@gmail.com>
*/

#include "wifi-radio-energy-stats-calculator.h"
#include "ns3/log.h"
#include "ns3/config.h"
#include "ns3/names.h"
#include <iostream>
#include "ns3/simulator.h"

NS_LOG_COMPONENT_DEFINE ("WifiRadioEnergyStatsCalculator");


namespace ns3 {

        WifiRadioEnergyStatsCalculator::WifiRadioEnergyStatsCalculator () 
		: m_firstWrite (true)
         {
			//NS_LOG_FUNCTION (this);
	 }

	WifiRadioEnergyStatsCalculator::~WifiRadioEnergyStatsCalculator ()
	{
		NS_LOG_FUNCTION (this);
	}

	TypeId
		WifiRadioEnergyStatsCalculator::GetTypeId (void)
	{
		static TypeId tid = TypeId ("ns3::WifiRadioEnergyStatsCalculator")
			.SetParent<WifiStatsCalculator> ()
			.AddConstructor<WifiRadioEnergyStatsCalculator> ()
			;
		return tid;
	}

	void
		WifiRadioEnergyStatsCalculator::WriteStatisticsToFile (Ptr<WifiNetDevice> wifiDevice, Time stateStart, WifiPhy::State oldState, double energyDecrease, double totalEnergyConsumption)
	{
		std::string sOldState = "INVALID";

		switch (oldState) {
		    case WifiPhy::IDLE:
                      sOldState = "IDLE";
                      break;
                    case WifiPhy::CCA_BUSY:
                      sOldState = "CCA_BUSY";
                      break;
                    case WifiPhy::TX:
                      sOldState = "TX";
                      break;
                    case WifiPhy::RX:
                      sOldState = "RX";
                      break;
                    case WifiPhy::SWITCHING:
                      sOldState = "SWITCHING";
                      break;
		    default:
			NS_FATAL_ERROR ("unknow state");
			break;
		}

               /* std::ofstream outFile;

                if (m_firstWrite == true) 
                {
                        NS_LOG_INFO("Write energy statistic in output file.");
                        outFile.open (GetOutputFilename().c_str());
                        if (!outFile.is_open())
                        {
                                NS_LOG_ERROR ("Can't open file");
                                return;
                        }
                        m_firstWrite = false;
                        outFile << "Time(s)\t TStart\t TDur\t tState\t energyDecrease\t totalEnergy\t";
                        outFile << std::endl;
                } else 
                {
                        outFile.open (GetOutputFilename().c_str(), std::ios_base::app);
                        if (!outFile.is_open())
                        {
                                NS_LOG_ERROR ("Can't open file");
                                return;
                        }                        
                }*/

		Time duration = Simulator::Now()-stateStart;
                Ptr<Node> node = wifiDevice->GetNode();
                uint32_t id = node->GetId();
               
                // Imprimindo dados XML
                NS_LOG_INFO ("\t\t<Wlan>\n\t\t\t<time>" <<  Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "</time>\n\t\t\t<start>"
                                << stateStart.GetNanoSeconds() / (double) 1e9 << "</start>\n\t\t\t<duration>" 
                                << duration.GetNanoSeconds() / (double) 1e9 << "</duration>\n\t\t\t<node>" 
                                << id << "</node>\n\t\t\t<state>" 
                                << sOldState << "</state>\n\t\t\t<energyDecrease>" << energyDecrease << "</energyDecrease>\n\t\t\t<totalEnergy>"
                                << totalEnergyConsumption << "</totalEnergy>\n\t\t</Wlan>");

		/*outFile << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t";
		outFile << stateStart.GetNanoSeconds() / (double) 1e9 << "\t";
		outFile << duration.GetNanoSeconds() / (double) 1e9 << "\t";
		outFile << sOldState << "\t";
		outFile << energyDecrease << "\t";
		outFile << totalEnergyConsumption << std::endl;
		outFile.close ();*/
	}

        void
        WifiRadioEnergyStatsCalculator::EnergyStatisticCallback(Ptr<WifiRadioEnergyModel> pModel, Time stateStart,double energyDecrease, WifiPhy::State oldState)
        {
          double totalEnergyConsumption = pModel->GetTotalEnergyConsumption();

          WriteStatisticsToFile(pModel->GetWifiNetDevice(), stateStart, oldState, energyDecrease, totalEnergyConsumption);
        }

}
