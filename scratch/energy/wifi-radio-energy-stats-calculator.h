/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Giovanna Garcia Basilio <ggarciabas@gmail.com>
*/

#ifndef WIFI_RADIO_ENERGY_STATS_CALCULATOR_H
#define WIFI_RADIO_ENERGY_STATS_CALCULATOR_H

#include "wifi-stats-calculator.h"
#include "ns3/wifi-phy.h"
#include "wifi-radio-energy-model.h"
#include "ns3/wifi-net-device.h"
#include <fstream>

namespace ns3 {
        
class WifiRadioEnergyStatsCalculator : public WifiStatsCalculator 
{
public:
        WifiRadioEnergyStatsCalculator();
        ~WifiRadioEnergyStatsCalculator();

        static TypeId GetTypeId ();
                      
        void WriteStatisticsToFile (Ptr<WifiNetDevice> wifiDevice, Time startState, WifiPhy::State oldState, double energyDecrease, double totalEnergyConsumption);

	void EnergyStatisticCallback (Ptr<WifiRadioEnergyModel> pModel, Time stateStart, double energyDecrease, WifiPhy::State oldState);

private:
	bool m_firstWrite;  
};

}
#endif // WIFI_RADIO_ENERGY_STATS_CALCULATOR_H
