/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Kun Wang <kun.wang@cttc.es>
*	  Giovanna Garcia Basilio <ggarciabas@gmail.com>
*/

#include "lte-radio-energy-model-helper.h"
#include "ns3/basic-energy-source-helper.h"
#include "ns3/lte-spectrum-phy.h"
#include "ns3/lte-net-device.h"
#include "ns3/config.h"
#include "ns3/names.h"
#include "ns3/lte-ue-net-device.h"
#include "ns3/lte-ue-phy.h"
#include "ns3/lte-phy.h"


NS_LOG_COMPONENT_DEFINE ("LteRadioEnergyModelHelper");

namespace ns3 {

	LteRadioEnergyStatsCalculator::LteRadioEnergyStatsCalculator () 
		: m_dlFirstWrite (true), m_ulFirstWrite (true) {
			//NS_LOG_FUNCTION (this);
	}

	LteRadioEnergyStatsCalculator::~LteRadioEnergyStatsCalculator ()
	{
		//NS_LOG_FUNCTION (this);
	}

	TypeId
		LteRadioEnergyStatsCalculator::GetTypeId (void)
	{
		static TypeId tid = TypeId ("ns3::LteRadioEnergyStatsCalculator")
			.SetParent<LteStatsCalculator> ()
			.AddConstructor<LteRadioEnergyStatsCalculator> ()
			;
		return tid;
	}

	void
		LteRadioEnergyStatsCalculator::WriteStatisticsToFile (Ptr<LteUeNetDevice> device, Time stateStart, uint64_t imsi,
		LteRadioEnergyModel::LinkType linktype, LteSpectrumPhy::State oldState,
		double energyDecrease, double totalEnergyConsumption,double TxPSD, double nRB)
	{
		NS_LOG_FUNCTION (this << linktype);
		std::string sLinkType  = "INVALID";
		//std::ofstream outFile;
		if (linktype == LteRadioEnergyModel::UPLINK){
			//NS_LOG_INFO ("Write energy statistic in " << GetUlOutputFilename ().c_str());
			sLinkType = "UPLINK";
			/*if (m_ulFirstWrite == true) {
				outFile.open (GetUlOutputFilename ().c_str());
				if (!outFile.is_open()) 
				{
					NS_LOG_ERROR("Can't open file " << GetUlOutputFilename ().c_str());
					return;
				}
				m_ulFirstWrite = false;
				outFile << "Time(s)\t TStart\t TDur\t imsi\t linkType\t tState\t energyDecrease\t totalEnergy\t txPSD\t nRB";
				outFile << std::endl;
			} else {
				outFile.open(GetUlOutputFilename ().c_str (), std::ios_base::app);
				if (!outFile.is_open())
				{
					NS_LOG_ERROR ("Can't open file " << GetUlOutputFilename().c_str());
					return;
				}
			}*/
		} else if (linktype == LteRadioEnergyModel::DOWNLINK) {
			//NS_LOG_INFO ("Write energy statistics in " << GetDlOutputFilename().c_str());
			sLinkType = "DONWLINK";
			/*if (m_dlFirstWrite == true) {
				outFile.open(GetDlOutputFilename().c_str());
				if (!outFile.is_open())
				{
					NS_LOG_ERROR ("can't open file " << GetDlOutputFilename().c_str());
					return;
				}
				m_dlFirstWrite = false;
				outFile << "Time(s)\t TStart\t TDur\t imsi\t linkType\t tState\t energyDecrease\t totalEnergy\t txPSD\t nRB";
				outFile << std::endl;
			} else {
				outFile.open (GetDlOutputFilename().c_str(), std::ios_base::app);
				if(!outFile.is_open())
				{
					NS_LOG_ERROR ("Can't open file " << GetDlOutputFilename().c_str());
					return;
				}
			}*/
		} else {
			NS_FATAL_ERROR ("Link Type is error!");
		}

		std::string sOldState = "INVALID";

		switch (oldState) {
		case LteSpectrumPhy::IDLE:
			sOldState = "IDLE";
			break;
		case LteSpectrumPhy::TX:
			sOldState = "TX";
			break;
		case LteSpectrumPhy::RX_DATA:
			sOldState = "RX_DATA";
			break;
		case LteSpectrumPhy::RX_CTRL:
			sOldState = "RX_CTRL";
			break;
		default:
			NS_FATAL_ERROR ("unknow state");
			break;
		}

		Time duration = Simulator::Now()-stateStart;
                Ptr<Node> node = device->GetNode();
                uint32_t id = node->GetId();

                // Imprimindo dados XML
                NS_LOG_INFO ("\t\t<Lte>\n\t\t\t<time>" <<  Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "</time>\n\t\t\t<start>"
                                << stateStart.GetNanoSeconds() / (double) 1e9 << "</start>\n\t\t\t<duration>" 
                                << duration.GetNanoSeconds() / (double) 1e9 << "</duration>\n\t\t\t<node>" 
                                << id << "</node>\n\t\t\t<imsi>"
                                << imsi << "</imsi>\n\t\t\t<link>" << sLinkType << "</link>\n\t\t\t<state>" 
                                << sOldState << "</state>\n\t\t\t<energyDecrease>" << energyDecrease << "</energyDecrease>\n\t\t\t<totalEnergy>"
                                << totalEnergyConsumption << "</totalEnergy>\n\t\t\t<txPSD>" << TxPSD << "</txPSD>\n\t\t\t<nRB>" << nRB << "</nRB>\n\t\t</Lte>");

		/*outFile << Simulator::Now ().GetNanoSeconds () / (double) 1e9 << "\t";
		outFile << stateStart.GetNanoSeconds() / (double) 1e9 << "\t";
		outFile << duration.GetNanoSeconds() / (double) 1e9 << "\t";
		outFile << imsi << "\t";
		outFile << sLinkType << "\t";
		outFile << sOldState << "\t";
		outFile << energyDecrease << "\t";
		outFile << totalEnergyConsumption << "\t";
		outFile << TxPSD << "\t";
		outFile << nRB << std::endl;
		outFile.close ();*/
	}

        void
        LteRadioEnergyStatsCalculator::EnergyStatisticCallback(Ptr<LteRadioEnergyModel> pModel,
		                               Time stateStart,double energyDecrease,LteSpectrumPhy::State oldState)
        {
          double totalEnergyConsumption = pModel->GetTotalEnergyConsumption();
          double TxPSD = pModel->GetTxPSD();
          LteRadioEnergyModel::LinkType linktype = pModel->GetLinkType();
          double nRB = pModel->GetTxRB();
          Ptr<LteUeNetDevice> pDevice = pModel->GetNetDevice();
          uint64_t imsi = pDevice->GetImsi();

          WriteStatisticsToFile(pModel->GetNetDevice(), stateStart, imsi, linktype, oldState, energyDecrease,
		                        totalEnergyConsumption,
		                        TxPSD, nRB );
        }

        //============================================================================================

        LteRadioEnergyModelHelper::LteRadioEnergyModelHelper ()
        {
          m_radioEnergy.SetTypeId ("ns3::LteRadioEnergyModel");
          m_depletionCallback.Nullify ();
          m_pLteRadioEnergyStatsCalculator = CreateObject<LteRadioEnergyStatsCalculator> ();
        }

        void LteRadioEnergyModelHelper::SetDate (std::string date) 
        {
           std::ostringstream path1;
           path1 << "scratch/energy/lte/" << date << "-DlEnergyStats.log";
           m_pLteRadioEnergyStatsCalculator->SetDlOutputFilename(path1.str());
           //std::cout << path1.str() << std::endl; 
           std::ostringstream path2;
           path2 << "scratch/energy/lte/" << date << "-UlEnergyStats.log";
           m_pLteRadioEnergyStatsCalculator->SetUlOutputFilename(path2.str());
           //std::cout << path2.str() << std::endl; 
        }

        LteRadioEnergyModelHelper::~LteRadioEnergyModelHelper ()
        {

        }

        void
        LteRadioEnergyModelHelper::Set (std::string name, const AttributeValue &v)
        {
          m_radioEnergy.Set (name, v);
        }

        void
        LteRadioEnergyModelHelper::SetDepletionCallback (
          LteRadioEnergyModel::LteRadioEnergyDepletionCallback callback)
        {
          m_depletionCallback = callback;
        }

        DeviceEnergyModelContainer
        LteRadioEnergyModelHelper::Install (NetDeviceContainer deviceContainer,
                                          EnergySourceContainer sourceContainer) const
        {
          NS_ASSERT (deviceContainer.GetN () <= sourceContainer.GetN ());
          DeviceEnergyModelContainer container;
          NetDeviceContainer::Iterator dev = deviceContainer.Begin ();
          EnergySourceContainer::Iterator src = sourceContainer.Begin ();
          while (dev != deviceContainer.End ())
            {
              // check to make sure source and net device are on the same node
              NS_ASSERT ((*dev)->GetNode () == (*src)->GetNode ());
              //Install the uplinkspectrum
              Ptr<DeviceEnergyModel> model = DoInstall (*dev, *src);
              container.Add (model);
              //Install the downlinkspectrum
              model = DoInstall (*dev, *src);
              container.Add (model);
              dev++;
              src++;
            }
          return container;
        }


        /*
         * Private function starts here.
         */

        Ptr<DeviceEnergyModel>
        LteRadioEnergyModelHelper::DoInstall (Ptr<NetDevice> device,
                                               Ptr<EnergySource> source) const
        {
          NS_ASSERT (device != NULL);
          NS_ASSERT (source != NULL);
          // check if device is lte NetDevice
          std::string deviceName = device->GetInstanceTypeId ().GetName ();
          if (deviceName.compare ("ns3::LteUeNetDevice") != 0)
            {
	          NS_FATAL_ERROR ("NetDevice type is not LteUeNetDevice!");
            }
          //check if the models have been installed.
          DeviceEnergyModelContainer findModels =
            source->FindDeviceEnergyModels ("ns3::LteRadioEnergyModel");
          bool ifUplinkSpectrum = false;
          bool ifDownlinkSpectrum = false;
          Ptr<LteRadioEnergyModel> model = 0;
          // check list
          for (DeviceEnergyModelContainer::Iterator i = findModels.Begin (); i != findModels.End (); ++i)
            {
	          Ptr<LteRadioEnergyModel> tempModel = DynamicCast<LteRadioEnergyModel> (*i);
              if (tempModel->GetLinkType() == LteRadioEnergyModel::UPLINK)
	            {
		          ifUplinkSpectrum = true;
	            }
	          else if (tempModel->GetLinkType() == LteRadioEnergyModel::DOWNLINK)
	            {
		          ifDownlinkSpectrum = true;
	            }
	          else
	          {
		          NS_FATAL_ERROR ("Lte Energy Model link type is error!");
	          }
            }
          if(ifUplinkSpectrum == false)
	        {
	          NS_LOG_FUNCTION("LTE RADIO ENERGY uplink model is not installed!");
	          model = m_radioEnergy.Create ()->GetObject<LteRadioEnergyModel> ();
	          NS_ASSERT (model != NULL);
	          // set energy source pointer
	          model->SetEnergySource (source);
	          // set energy depletion callback
	          model->SetEnergyDepletionCallback (m_depletionCallback);
              Callback<void, Ptr<LteRadioEnergyModel>, Time,double,LteSpectrumPhy::State> callback =
	              MakeCallback (&LteRadioEnergyStatsCalculator::EnergyStatisticCallback,
			                    m_pLteRadioEnergyStatsCalculator);
	          model->SetEnergyStatisticCallback(callback);
	          // add model to device model list in energy source
	          source->AppendDeviceEnergyModel (model);
	          // create and register energy model phy listener
	          Ptr<LteUePhy> uePhy = device->GetObject<LteUeNetDevice> ()->GetPhy ();
	          Ptr<LteSpectrumPhy> LteUplinkSpectrumPhy = uePhy->GetUplinkSpectrumPhy();
	          LteUplinkSpectrumPhy->RegisterListener (model->GetPhyListener ());
	          model->SetLinkType(LteRadioEnergyModel::UPLINK);
	          model->SetNetDevice(DynamicCast<LteUeNetDevice>(device));
	          return model;
	        }
          else if (ifDownlinkSpectrum  == false)
            {
	          NS_LOG_FUNCTION("LTE RADIO ENERGY downlink model is not installed!");
	          model = m_radioEnergy.Create ()->GetObject<LteRadioEnergyModel> ();
	          NS_ASSERT (model != NULL);
	          // set energy source pointer
	          model->SetEnergySource (source);
	          // set energy depletion callback
	          model->SetEnergyDepletionCallback (m_depletionCallback);
	          Callback<void, Ptr<LteRadioEnergyModel>, Time,double,LteSpectrumPhy::State> callback =
	          	   MakeCallback (&LteRadioEnergyStatsCalculator::EnergyStatisticCallback,
	          			         m_pLteRadioEnergyStatsCalculator);
	          model->SetEnergyStatisticCallback(callback);
	          // add model to device model list in energy source
	          source->AppendDeviceEnergyModel (model);
	          // create and register energy model phy listener
	          Ptr<LteUePhy> uePhy = device->GetObject<LteUeNetDevice> ()->GetPhy ();
	          Ptr<LteSpectrumPhy> LteDownlinkSpectrumPhy = uePhy->GetDownlinkSpectrumPhy();
	          LteDownlinkSpectrumPhy->RegisterListener (model->GetPhyListener ());
	          model->SetLinkType(LteRadioEnergyModel::DOWNLINK);
	          model->SetNetDevice(DynamicCast<LteUeNetDevice>(device));
	          return model;
            }
          else
            {
	          NS_FATAL_ERROR ("Lte Radio Energy Model has been installed!");
	          return 0;
            }
        }

}
