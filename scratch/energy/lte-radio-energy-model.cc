/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Authors: Kun Wang <kun.wang@cttc.es>
*	   Giovanna Garcia Basilio <ggarciabas@gmail.com>
*/

#include "ns3/log.h"
#include "ns3/double.h"
#include "ns3/simulator.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/energy-source.h"
#include "lte-radio-energy-model.h"
#include <fstream>

NS_LOG_COMPONENT_DEFINE ("LteRadioEnergyModel");

namespace ns3 {
	NS_OBJECT_ENSURE_REGISTERED (LteRadioEnergyModel);

	TypeId LteRadioEnergyModel::GetTypeId (void) {
		static TypeId typeId = TypeId ("ns3::LteRadioEnergyModel")
			.SetParent<DeviceEnergyModel> ()
			.AddConstructor<LteRadioEnergyModel> ()
			. AddAttribute ("IdleCurrentA",
							"The default radio Idle current in Ampere.",
							DoubleValue (0.000426), // idle mode = 426uA
							MakeDoubleAccessor (&LteRadioEnergyModel::SetIdleCurrentA,
												&LteRadioEnergyModel::GetIdleCurrentA),
							MakeDoubleChecker<double> ())
			.AddAttribute ("RxDataCurrentA",
							"The radio RX_DATA current in Ampere.",
							DoubleValue (0.0197), // receive data mode = 19.7mA
							MakeDoubleAccessor (&LteRadioEnergyModel::SetRxDataCurrentA,
												&LteRadioEnergyModel::GetRxDataCurrentA),
							MakeDoubleChecker<double>())
		    .AddAttribute ("RxCtrlCurrentA",
							"The radio RX_CTRL current in Ampere.",
							DoubleValue(0.0197), // receive control mode = 19.7mA
							MakeDoubleAccessor (&LteRadioEnergyModel::SetRxCtrlCurrentA,
												&LteRadioEnergyModel::GetRxCtrlCurrentA),
							MakeDoubleChecker<double>())
			.AddAttribute ("TxCurrentA",
							"The radio TX current in Ampere.",
							DoubleValue(0.013333), // transmit at 23dBm = 1w
							MakeDoubleAccessor (&LteRadioEnergyModel::SetTxCurrentA,
												&LteRadioEnergyModel::GetTxCurrentA),
							MakeDoubleChecker<double>())
			/*.AddAttribute ("TxDataFrame",
							"The radio TX current in Ampere.",
							DoubleValue(0.013333), // transmit at 23dBm = 1w
							MakeDoubleAccessor (&LteRadioEnergyModel::SetTxDataFrameCurrentA,
												&LteRadioEnergyModel::GetTxDataFrameCurrentA),
							MakeDoubleChecker<double>())
		    .AddAttribute ("TxDlCtrlFrame",
							"The radio TX current in Ampere.",
							DoubleValue(0.013333), // transmit at 23dBm = 1w
							MakeDoubleAccessor (&LteRadioEnergyModel::SetTxDlCtrlFrameCurrentA,
												&LteRadioEnergyModel::GetTxDlCtrlFrameCurrentA),
							MakeDoubleChecker<double>())
			.AddAttribute ("TxUlSrsFrame",
							"The radio TX current in Ampere.",
							DoubleValue(0.013333), // transmit at 23dBm = 1w
							MakeDoubleAccessor (&LteRadioEnergyModel::SetTxUlSrsFrameCurrentA,
												&LteRadioEnergyModel::GetTxUlSrsFrameCurrentA),
							MakeDoubleChecker<double>())*/
			.AddTraceSource ("TotalEnergyConsumption", 
							 "Total energy consumption of the radio device.", 
							 MakeTraceSourceAccessor(&LteRadioEnergyModel::m_totalEnergyConsumption));

		return typeId;
	}

	LteRadioEnergyModel::LteRadioEnergyModel ()
	{
		NS_LOG_FUNCTION (this);
		m_currentState = LteSpectrumPhy::IDLE; // initially IDLE
		m_lastUpdateTime = Seconds (0.0);
		m_energyDepletionCallback.Nullify();
		m_source = NULL;
		m_pDevice = NULL;
		m_txPSD = 0;
		m_txPSD = 0;
		/*m_txDataFramePSD = 0;
		m_txDlCtrlFramePSD = 0;
		m_txDlCtrlFrameRB = 0;
		m_txDataFrameRB = 0;
		m_txDlCtrlFrameRB = 0;
		m_txUlSrsFrameRB = 0;*/
		m_listener = new LteRadioEnergyModelPhyListener;
		m_listener->SetChangeStateCallback(MakeCallback (&DeviceEnergyModel::ChangeState, this));
		m_listener->SetPowerSpectralDensityCallback(MakeCallback(&LteRadioEnergyModel::SetTxPSD_RB, this));
	}

	LteRadioEnergyModel::~LteRadioEnergyModel ()
	{
		delete m_listener;
	}	

	void LteRadioEnergyModel::SetEnergySource (Ptr<EnergySource> source) 
	{
		NS_LOG_FUNCTION (this << source);
		NS_ASSERT (source != NULL);
		m_source = source;
	}

	double LteRadioEnergyModel::GetTotalEnergyConsumption(void) const
	{
		return m_totalEnergyConsumption;
	}

	double LteRadioEnergyModel::GetIdleCurrentA (void) const
	{
		return m_idleCurrentA;
	}

	void LteRadioEnergyModel::SetIdleCurrentA (double idleCurrentA) 
	{
		NS_LOG_FUNCTION (this << idleCurrentA);
		m_idleCurrentA = idleCurrentA;
	}

	double LteRadioEnergyModel::GetRxDataCurrentA (void) const
	{
		return m_rxDataCurrentA;
	}

	void LteRadioEnergyModel::SetRxDataCurrentA (double rxDataCurrentA)
	{
		NS_LOG_FUNCTION (this << rxDataCurrentA);
		m_rxDataCurrentA = rxDataCurrentA;
	}

	double LteRadioEnergyModel::GetRxCtrlCurrentA (void) const
	{
		return m_rxCtrlCurrentA;
	}

	void LteRadioEnergyModel::SetRxCtrlCurrentA (double rxCtrlCurrentA)
	{
		NS_LOG_FUNCTION (this << rxCtrlCurrentA);
		m_rxCtrlCurrentA = rxCtrlCurrentA;
	}

	double LteRadioEnergyModel::GetTxCurrentA (void) const
	{
		return m_txCurrentA;
	}

	void LteRadioEnergyModel::SetTxCurrentA (double txCurrentA)
	{
		NS_LOG_FUNCTION (this << txCurrentA);
		m_txCurrentA = txCurrentA;
	}

	/*double LteRadioEnergyModel::GetTxDataFrameCurrentA (void) const
	{
		return m_txDataFrameCurrentA;
	}

	void LteRadioEnergyModel::SetTxDataFrameCurrentA (double txDataFrameCurrentA)
	{
		NS_LOG_FUNCTION (this << txDataFrameCurrentA);
		m_txDataFrameCurrentA = txDataFrameCurrentA;
	}

	double LteRadioEnergyModel::GetTxDlCtrlFrameCurrentA (void) const
	{
		return m_txDlCtrlFrameCurrentA;
	}

	void LteRadioEnergyModel::SetTxDataFrameCurrentA (double txDlCtrlFrameCurrentA)
	{
		NS_LOG_FUNCTION (this << txDlCtrlFrameCurrentA);
		m_txDlCtrlFrameCurrentA = txDlCtrlFrameCurrentA;
	}

	double LteRadioEnergyModel::GetTxUlSrsFrameCurrentA (void) const
	{
		return m_txUlSrsFrameCurrentA;
	}

	void LteRadioEnergyModel::SetTxUlSrsFrameCurrentA (double txUlSrsFrameCurrentA)
	{
		NS_LOG_FUNCTION (this << txUlSrsFrameCurrentA);
		m_txUlSrsFrameCurrentA = txUlSrsFrameCurrentA;
	}*/

	LteSpectrumPhy::State LteRadioEnergyModel::GetCurrentState (void) const 
	{
		return m_currentState;
	}

	double LteRadioEnergyModel::CalculateTxCurrentA (double txPSD, long nRB) 
	{
		if (m_source == 0) 
		{
			NS_LOG_FUNCTION (this << "m_source : " << m_source);
			return 0;
		} else 
		{
			double supplyVoltage = m_source->GetSupplyVoltage ();
			double txPower = txPSD * nRB * 180000;
			double txCurrentA = txPower/supplyVoltage;
			return txCurrentA;
		}
	}

	void LteRadioEnergyModel::SetTxPSD_RB (double txPSD, double nRB)
	{
		NS_LOG_FUNCTION (this << txPSD << nRB);
		m_txPSD = txPSD;
		m_txRB = nRB;
		m_txCurrentA = (double) CalculateTxCurrentA(m_txPSD, m_txRB);
	}

	double LteRadioEnergyModel::GetTxPSD (void) const
	{
		return m_txPSD;
	}

	double LteRadioEnergyModel::GetTxRB (void) const
	{
		return m_txRB;
	}

	/*void LteRadioEnergyModel::SetTxDataFramePSD_RB (double txPSD, double nRB)
	{
		NS_LOG_FUNCTION (this << txPSD << nRB);
		m_txDataFramePSD = txPSD;
		m_txDataFrameRB = nRB;
		m_txDataFrameCurrentA = CalculateTxCurrentA(m_txDataFramePSD, m_txDataFrameRB);
	}

	double LteRadioEnergyModel::GetTxDataFramePSD (void) const
	{
		return m_txDataFramePSD;
	}

	double LteRadioEnergyModel::GetTxDataFrameRB (void) const
	{
		return m_txDataFrameRB;
	}

	void LteRadioEnergyModel::SetTxDlCtrlFramePSD_RB (double txPSD, double nRB)
	{
		NS_LOG_FUNCTION (this << txPSD << nRB);
		m_txDlCtrlFramePSD = txPSD;
		m_txDlCtrlFrameRB = nRB;
		m_txDlCtrlFrameCurrentA = CalculateTxCurrentA(m_txDlCtrlFramePSD, m_txDlCtrlFrameRB);
	}

	double LteRadioEnergyModel::GetTxDlCtrlFramePSD (void) const
	{
		return m_txDlCtrlFramePSD;
	}

	double LteRadioEnergyModel::GetTxDlCtrlFrameRB (void) const
	{
		return m_txDlCtrlFrameRB;
	}

	void LteRadioEnergyModel::SetTxUlSrsFramePSD_RB (double txPSD, double nRB)
	{
		NS_LOG_FUNCTION (this << txPSD << nRB);
		m_txUlSrsFramePSD = txPSD;
		m_txUlSrsFrameRB = nRB;
		m_txUlSrsFrameCurrentA = CalculateTxCurrentA(m_txUlSrsFramePSD, m_txUlSrsFrameRB);
	}

	double LteRadioEnergyModel::GetTxUlSrsFramePSD (void) const
	{
		return m_txUlSrsFramePSD;
	}

	double LteRadioEnergyModel::GetTxUlSrsFrameRB (void) const
	{
		return m_txUlSrsFrameRB;
	}*/

	void LteRadioEnergyModel::SetEnergyDepletionCallback (LteRadioEnergyDepletionCallback callback)
	{
		NS_LOG_FUNCTION (this);
		if (callback.IsNull ())
		{
			NS_LOG_DEBUG("LteRadioEnergyModel: Setting NULL energy depletion callback!");
		}
		m_energyDepletionCallback = callback;
	}

	void LteRadioEnergyModel::SetNetDevice (Ptr<LteUeNetDevice> pDevice)
	{
		m_pDevice = pDevice;
	}

	Ptr<LteUeNetDevice> LteRadioEnergyModel::GetNetDevice ()
	{
		return m_pDevice;
	}

	void LteRadioEnergyModel::SetEnergyStatisticCallback (Callback<void, Ptr<LteRadioEnergyModel>, Time, double, LteSpectrumPhy::State> callback)
	{
		m_energyStatistic = callback;
	}

	void LteRadioEnergyModel::ChangeState (int newState) 
	{
		NS_LOG_FUNCTION (this << newState);
		double energyToDecrease = 0.0;
		double supplyVoltage = m_source->GetSupplyVoltage();
		Time duration = Simulator::Now() - m_lastUpdateTime;
		NS_ASSERT (duration.GetNanoSeconds() >= 0); // check if duration is valid
		// energy to decrease = current * voltage * time
		switch (m_currentState)
		{
		case LteSpectrumPhy::IDLE:
			energyToDecrease = duration.GetSeconds() * m_idleCurrentA * supplyVoltage;
			break;
		case LteSpectrumPhy::RX_DATA:
			energyToDecrease = duration.GetSeconds() * m_rxDataCurrentA * supplyVoltage;
			break;
		case LteSpectrumPhy::RX_CTRL:
			energyToDecrease = duration.GetSeconds() * m_rxCtrlCurrentA * supplyVoltage;
			break;
		case LteSpectrumPhy::TX:
			energyToDecrease = duration.GetSeconds() * m_txCurrentA * supplyVoltage;
			break;
		default:
			NS_FATAL_ERROR ("LteRadioEnergyModel:Undefined radio state: " << m_currentState);
		}

		// send statistics 
		m_energyStatistic (this, m_lastUpdateTime, energyToDecrease, m_currentState);
		// update last update time stamp
		m_lastUpdateTime = Simulator::Now();
		// update total energy consumption
		m_totalEnergyConsumption += energyToDecrease;

		// notify energy source
		m_source->UpdateEnergySource();

		// update current state & last update time stamp
  		SetLteRadioState ((LteSpectrumPhy::State) newState);  

		// some debug message
		NS_LOG_DEBUG ("LteRadioEnergyModel:Total energy consumption is " << m_totalEnergyConsumption << "J");
	}

	void LteRadioEnergyModel::HandleEnergyDepletion (void)
	{
		NS_LOG_DEBUG ("LteRadioEnergyModel:Energy is depleted!");
		// invoke energy depletion callback, if set.
		if (!m_energyDepletionCallback.IsNull())
		{
			m_energyDepletionCallback ();
		}
	}

	LteRadioEnergyModelPhyListener * LteRadioEnergyModel::GetPhyListener (void)
	{
		return m_listener;
	}

	void LteRadioEnergyModel::DoDispose (void) 
	{
		m_source = NULL;
		m_energyDepletionCallback.Nullify ();
	}

	void LteRadioEnergyModel::SetLteRadioState (const LteSpectrumPhy::State state)
	{
		NS_LOG_FUNCTION (this << state);

		m_currentState = state;
		std::string stateName;
		switch (state)
		{
		case LteSpectrumPhy::IDLE:
			stateName = "IDLE";
			break;
		case LteSpectrumPhy::RX_DATA:
			stateName = "RX_DATA";
			break;
		case LteSpectrumPhy::RX_CTRL:
			stateName = "RX_CTRL";
			break;
		case LteSpectrumPhy::TX:
			stateName = "TX";
			break;
		}
		NS_LOG_DEBUG ("LteRadioEnergyModel:Switching to state: " << stateName << " at time = " << Simulator::Now());
	}

	void LteRadioEnergyModel::SetLinkType (LinkType linkType)
	{
		m_linkType = linkType;
	}

	LteRadioEnergyModel::LinkType LteRadioEnergyModel::GetLinkType (void) 
	{
		return m_linkType;
	}

        double
        LteRadioEnergyModel::DoGetCurrentA (void) const
        {
          NS_LOG_FUNCTION (this);
          switch (m_currentState)
            {
            case LteSpectrumPhy::IDLE:
              return m_idleCurrentA;
            case LteSpectrumPhy::TX:
              return m_txCurrentA;
            case LteSpectrumPhy::RX_DATA:
              return m_rxDataCurrentA;
            case LteSpectrumPhy::RX_CTRL:
              return m_rxCtrlCurrentA;
            default:
              NS_FATAL_ERROR ("LteRadioEnergyModel:Undefined radio state:" << m_currentState);
            }
        }

	// LteRadioEnergyModelPhyListener ------------- implementations

	LteRadioEnergyModelPhyListener::LteRadioEnergyModelPhyListener()
	{
		m_changeStateCallback.Nullify();
	}

	LteRadioEnergyModelPhyListener::~LteRadioEnergyModelPhyListener()
	{
		
	}

	void LteRadioEnergyModelPhyListener::SetChangeStateCallback (DeviceEnergyModel::ChangeStateCallback callback)
	{
		NS_ASSERT (!callback.IsNull());
		m_changeStateCallback = callback;
	}

	void LteRadioEnergyModelPhyListener::SetPowerSpectralDensityCallback (PowerSpectralDensityCallback callback)
	{
		NS_ASSERT (!callback.IsNull());
		m_powerSpectralDensityCallback = callback;
	}

	void LteRadioEnergyModelPhyListener::NotifyRxDataStart (void)
	{
		if (m_changeStateCallback.IsNull())
		{
			NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:Change state callback not set!");
		}
		m_changeStateCallback (LteSpectrumPhy::RX_DATA);
		m_switchToIdleEvent.Cancel ();
	}

	void LteRadioEnergyModelPhyListener::NotifyRxCtrlStart (void)
	{
		if (m_changeStateCallback.IsNull())
		{
			NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:Change state callback not set!");
		}
		m_changeStateCallback (LteSpectrumPhy::RX_CTRL);
		m_switchToIdleEvent.Cancel ();
	}

	void LteRadioEnergyModelPhyListener::NotifyRxDataEnd (void)
	{
		if (m_changeStateCallback.IsNull())
		{
			NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:Change state callback not set!");
		}
		m_changeStateCallback (LteSpectrumPhy::IDLE);
	}

	void LteRadioEnergyModelPhyListener::NotifyRxDlCtrlEnd (void)
	{
		if (m_changeStateCallback.IsNull())
		{
			NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:Change state callback not set!");
		}
		m_changeStateCallback (LteSpectrumPhy::IDLE);
	}

	void LteRadioEnergyModelPhyListener::NotifyRxUlSrsEnd (void)
	{
		if (m_changeStateCallback.IsNull())
		{
			NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:Change state callback not set!");
		}
		m_changeStateCallback (LteSpectrumPhy::IDLE);
	}

	void LteRadioEnergyModelPhyListener::NotifyTxStart (void)
	{
		if (m_changeStateCallback.IsNull())
		{
			NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:Change state callback not set!");
		}
		m_changeStateCallback (LteSpectrumPhy::TX);
		m_switchToIdleEvent.Cancel();
		m_switchToIdleEvent = Simulator::Schedule(MilliSeconds(1), &LteRadioEnergyModelPhyListener::SwitchToIdle, this); // programa o retorno para IDLE em 1ms 
	}

	void LteRadioEnergyModelPhyListener::NotifyNewTxPSD_RB (double newPsd, double nRB)
	{
		if (m_powerSpectralDensityCallback.IsNull ())
		{
			NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:power spectral Density callback not set!");
		}
		m_powerSpectralDensityCallback (newPsd, nRB);
	}

	void LteRadioEnergyModelPhyListener::SwitchToIdle (void)
	{
	  if (m_changeStateCallback.IsNull ())
		{
		  NS_FATAL_ERROR ("LteRadioEnergyModelPhyListener:Change state callback not set!");
		}
	  m_changeStateCallback (LteSpectrumPhy::IDLE);
	}
} // namespace ns3














