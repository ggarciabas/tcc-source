/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Authors: Kun Wang <kun.wang@cttc.es>
*			Giovanna Garcia Basilio <ggarciabas@gmail.com>
*/

#ifndef LTE_RADIO_ENERGY_MODEL_H
#define LTE_RADIO_ENERGY_MODEL_H

#include "ns3/device-energy-model.h"
#include "ns3/nstime.h"
#include "ns3/event-id.h"
#include "ns3/traced-value.h"
#include "ns3/lte-spectrum-phy.h"
#include "ns3/lte-ue-net-device.h"

namespace ns3 {

	/**
	* \ingroup energy
	* A LteUePhy listener class for notifying the LteRadioEnergyModel of Lte radio
	* state change.
	*
	*/
	class LteRadioEnergyModelPhyListener : public LteSpectrumPhyListener
	{
	public:
		LteRadioEnergyModelPhyListener();
		virtual ~LteRadioEnergyModelPhyListener();
		typedef Callback<void, double, double> PowerSpectralDensityCallback;
		/**
		* \brief Sets the change state callback. Used by helper class.
		*
		* \param callback Change state callback.
		*/
		void SetChangeStateCallback (DeviceEnergyModel::ChangeStateCallback callback);
		void SetPowerSpectralDensityCallback (LteRadioEnergyModelPhyListener::PowerSpectralDensityCallback callback);
		/**
		* \brief Switches the LteRadioEnergyModel to RX_DATA state.
		*
		* Defined in ns3::LteSpectrumPhyListener
		*/
		virtual void NotifyRxDataStart (void);
		/**
		* \brief Switches the LteRadioEnergyModel to IDLE state.
		*
		* Defined in ns3::LteSpectrumPhyListener
		*/
		virtual void NotifyRxDataEnd (void);
		/**
		* \brief Switches the LteRadioEnergyModel to RX_CTRL state.
		*
		* Defined in ns3::LteSpectrumPhyListener
		*/
		virtual void NotifyRxCtrlStart (void);
		virtual void NotifyRxDlCtrlEnd (void);
		virtual void NotifyRxUlSrsEnd (void);
		/**
		* \brief Switches the LteRadioEnergyModel to TX state and switches back to
		* IDLE after TX duration. (1ms)
		*
		* Defined in ns3::LteSpectrumPhyListener
		*/
		/*virtual void NotifyTxDataFrameStart (void);
		virtual void NotifyNewTxDataFramePSD (double newPsd, double nRB);
		virtual void NotifyTxDlCtrlFrameStart (void);
		virtual void NotifyNewTxDlCtrlFramePSD (double newPsd, double nRB);
		virtual void NotifyTxUlSrsFrameStart (void);
		virtual void NotifyNewTxUlSrsFramePSD (double newPsd, double nRB);*/
		virtual void NotifyTxStart (void);
		virtual void NotifyNewTxPSD_RB (double newPsd, double nRB);

	private:
		/**
		* A helper function that makes scheduling m_changeStateCallback possible.
		*/
		void SwitchToIdle (void);
		/**
		* Change state callback used to notify the LteRadioEnergyModel of a state
		* change.
		*/
		DeviceEnergyModel::ChangeStateCallback m_changeStateCallback;
		PowerSpectralDensityCallback m_powerSpectralDensityCallback;
		EventId m_switchToIdleEvent;
	};

	/**
	* \ingroup energy
	* \brief A Lte radio energy model.
	* 
	* 5 states are defined for the radio: TX, RX_DATA, RX_CTRL and IDLE. Default state is
	* IDLE.
	* The different types of transactions that are defined are: 
	*  1. Tx: State goes from IDLE to TX, radio is in TX state for TX_duration,
	*     then state goes from TX to IDLE.
	*  2. Rx_data: State goes from IDLE to RX_DATA, radio is in RX_DATA state for RX_DATA_duration,
	*     then state goes from RX_DATA to IDLE.
	*  3. Rx_ctrl: State goes from IDEL to RX_CTRL, radio is in RX_CTRL state for RX_CTRL_duration,
	*	  then state goes from RX_CTRL to IDLE.
	*
	* The class keeps track of what state the radio is currently in.
	*
	* Energy calculation: For each transaction, this model notifies EnergySource
	* object. The EnergySource object will query this model for the total current.
	* Then the EnergySource object uses the total current to calculate energy.
	*
	* TODO - verificar estes valores
	*
	* Default values for power consumption are based on ???????,
	* with supply voltage as ?? V and currents as ??? mA (TX), ??? mA (RX), ?? uA
	* (sleep) and ??? uA (idle).
	*
	*/
	class LteRadioEnergyModel : public DeviceEnergyModel
	{
	public:
		/**
		* Callback type for energy depletion handling.
		*/
		typedef Callback<void> LteRadioEnergyDepletionCallback;

		static TypeId GetTypeId (void);
		LteRadioEnergyModel ();
		virtual ~LteRadioEnergyModel ();
		enum LinkType
		{
			UPLINK, DOWNLINK
		};
		void SetLinkType (LinkType linktype);
		LinkType GetLinkType (void);
		/**
		* \brief Sets pointer to EnergySouce installed on node.
		*
		* \param source Pointer to EnergySource installed on node.
		*
		* Implements DeviceEnergyModel::SetEnergySource.
		*/
		virtual void SetEnergySource (Ptr<EnergySource> source);
		/**
		* \returns Total energy consumption of the lte device.
		*
		* Implements DeviceEnergyModel::GetTotalEnergyConsumption.
		*/
		virtual double GetTotalEnergyConsumption (void) const;

		// Setter & getter for state power consumption.
		double GetIdleCurrentA (void) const;
		void SetIdleCurrentA (double);
		double GetRxDataCurrentA (void) const;
		void SetRxDataCurrentA (double);
		double GetRxCtrlCurrentA (void) const;
		void SetRxCtrlCurrentA (double);
		double GetTxCurrentA (void) const;
		void SetTxCurrentA (double);
		/*double GetTxDataFrameCurrentA (void) const;
		void SetTxDataFrameCurrentA (double);
		double GetTxDlCtrlFrameCurrentA (void) const;
		void SetTxDlCtrlFrameCurrentA (double);
		double GetTxUlSrsFrameCurrentA (void) const;
		void SetTxUlSrsFrameCurrentA (double);*/
		// PSD = PowerSpectralDensity
		double GetTxPSD (void) const;
		double GetTxRB (void) const;
		void SetTxPSD_RB (double txPSD, double nRB);
		/*double GetTxDataFramePSD (void) const;
		double GetTxDataFrameRB (void) const;
		void SetTxDataFramePSD_RB (double txPSD, double nRB);
		double GetTxDlCtrlFramePSD (void) const;
		double GetTxDlCtrlFrameRB (void) const;
		void SetTxDlCtrlFramePSD_RB (double txPSD, double nRB);
		double GetTxUlSrsFramePSD (void) const;
		double GetTxUlSrsFrameRB (void) const;
		void SetTxUlSrsFramePSD_RB (double txPSD, double nRB);*/

		/**
		* \returns Current state.
		*/
		LteSpectrumPhy::State GetCurrentState (void) const;

		/**
		* \param callback Callback function.
		*
		* Sets callback for energy depletion handling.
		*/
		void SetEnergyDepletionCallback (LteRadioEnergyDepletionCallback callback);

		/**
		* \brief Changes state of the LteRadioEnergyMode.
		*
		* \param newState New state the lte radio is in.
		*
		* Implements DeviceEnergyModel::ChangeState.
		*/
		virtual void ChangeState (int newState);

		/**
		* \brief Handles energy depletion.
		*
		* Implements DeviceEnergyModel::HandleEnergyDepletion
		*/
		virtual void HandleEnergyDepletion (void);

		/**
		* \returns Pointer to the PHY listener.
		*/
		LteRadioEnergyModelPhyListener * GetPhyListener (void);
		void SetEnergyStatisticCallback (Callback<void, Ptr<LteRadioEnergyModel>, 
			Time, double, LteSpectrumPhy::State> callback);

		void SetNetDevice(Ptr<LteUeNetDevice> pDevice);
		Ptr<LteUeNetDevice> GetNetDevice(void);

	private:
		void DoDispose (void);

                /**
                  * \returns Current draw of device, at current state.
                  *
                  * Implements DeviceEnergyModel::GetCurrentA.
                  */
                virtual double DoGetCurrentA (void) const;

		/**
		* \param currentState New state the radio device is currently in.
		*
		* Sets current state. This function is private so that only the energy model
		* can change its own state.
		*/
		void SetLteRadioState (const LteSpectrumPhy::State state);

		/**
		* \param txPSD: New Transmit Power Spectral Density.
		* \param nRB: New allocated Resource Block to transmit on.
		*/
		double CalculateTxCurrentA (double txPSD, long nRB);

		Ptr<EnergySource> m_source;

		// Member variables for current draw in different radio modes
		double m_idleCurrentA;
		double m_rxDataCurrentA;
		double m_rxCtrlCurrentA;
		double m_txPSD;
		double m_txRB;
		double m_txCurrentA;
		/*double m_txDataFrameCurrentA;
		double m_txDlCtrlFrameCurrentA;
		double m_txUlSrsFrameCurrentA;
		double m_txDlCtrlFramePSD;
		double m_txDlCtrlFrameRB;
		double m_txUlSrsFramePSD;
		double m_txUlSrsFrameRB;
		double m_txDataFramePSD;
		double m_txDataFrameRB;*/

		// This variable keeps track of the total energy consumed by this model.
		TracedValue<double> m_totalEnergyConsumption;

		// State variables.
		LteSpectrumPhy::State m_currentState;
		// time stamp of previous energy update
		Time m_lastUpdateTime;

		// Energy depletion callback
		LteRadioEnergyDepletionCallback m_energyDepletionCallback;

		// LtePhy listener
		LteRadioEnergyModelPhyListener *m_listener;

		//Link type
		LinkType m_linkType;

		/**
		* Trace information regarding energy
		* Ptr to energy model, state start time,
		* energy to decrease, old state
		*/
		Callback<void, Ptr<LteRadioEnergyModel>, Time, double, LteSpectrumPhy::State> m_energyStatistic;
		Ptr<LteUeNetDevice> m_pDevice;
	};

} // namespace ns3

#endif /* LTE_RADIO_ENERGY_MODEL_H */
