#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# 
# Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
#

principal()
{
        clear
#        echo -n " Informe o teste: "
#	read i
        for i in {1..6}
	do
#		OPCAO = $i     		
		simula $i
	done
}

simula()
{
#        clear
        DATA=`date +%D`
        TEMPO=`date +%r`

        echo "Executando simulacao $1 $DATA $TEMPO ......."
#        ../../waf --run energy --command-template="gdb --args %s --teste=$OPCAO --data=$DATA-$TEMPO" 
	touch log/teste-$1/teste-$1.xml
	../../waf --run "energy --teste=$1" >& "log/teste-$1/teste-$1.xml"
#        echo "Simulação finalizada!"            
}

principal;

