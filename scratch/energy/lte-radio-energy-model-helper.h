/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Kun Wang <kun.wang@cttc.es>
*	  Giovanna Garcia Basilio <ggarciabas@gmail.com>
*/

#ifndef LTE_RADIO_ENERGY_MODEL_HELPER_H
#define LTE_RADIO_ENERGY_MODEL_HELPER_H

#include "ns3/energy-model-helper.h"
#include "lte-radio-energy-model.h"
#include "ns3/log.h"
#include "ns3/lte-stats-calculator.h"
#include <fstream>
#include <ns3/simulator.h>


namespace ns3 {

	/**
	* \ingroup energy
	* \brief Print LteRadioEnergyModel statistics to appointed files.
	*
	*
	*/
	class LteRadioEnergyStatsCalculator : public LteStatsCalculator
	{
	public:
		/**
		* Constructor
		*/
		LteRadioEnergyStatsCalculator ();

		/**
		* Destructor
		*/
		virtual ~LteRadioEnergyStatsCalculator ();

		/**
		* Inherited from ns3::Object
		*/
		static TypeId GetTypeId (void);

		void WriteStatisticsToFile (Ptr<LteUeNetDevice> device, Time startState, uint64_t imsi, 
			LteRadioEnergyModel::LinkType linkType, LteSpectrumPhy::State oldState,
			double energyDecrease, double totalEnergyConsumption, double txPSD, double nRB);

		void EnergyStatisticCallback (Ptr<LteRadioEnergyModel> pModel, 
			Time stateStart, double energyDecrease, LteSpectrumPhy::State oldState);

	private:
		bool m_dlFirstWrite;
		bool m_ulFirstWrite;
	};

	/**
	* \ingroup energy
	* \brief Assign LteRadioEnergyModel to lte devices.
	*
	* This installer installs LteRadioEnergyModel for only LteNetDevice objects.
	*
	*/
	class LteRadioEnergyModelHelper : public DeviceEnergyModelHelper
	{
	public:
		/**
		* Construct a helper which is used to add a radio energy model to a node
		*/
		LteRadioEnergyModelHelper ();

		/**
		* Destroy a RadioEnergy Helper
		*/
		~LteRadioEnergyModelHelper ();

		/**
		* \param name the name of the attribute to set
		* \param v the value of the attribute
		*
		* Sets an attribute of the underlying PHY object.
		*/
		void Set (std::string name, const AttributeValue &v);

		/**
		* \param callback Callback function for energy depletion handling.
		*
		* Sets the callback to be invoked when energy is depleted.
		*/
		void SetDepletionCallback (LteRadioEnergyModel::LteRadioEnergyDepletionCallback callback);

		/**
		* \param deviceContainer List of NetDevices to be install DeviceEnergyModel
		* objects.
		* \param sourceContainer List of EnergySource the DeviceEnergyModel will be
		* using.
		* \returns An DeviceEnergyModelContainer contains all the DeviceEnergyModels.
		*
		* Installs DeviceEnergyModels with specified EnergySources onto a list of
		* NetDevices.
		* It is to fulfill the requirement of two spectrums in one lte device
		*/
		DeviceEnergyModelContainer Install (NetDeviceContainer deviceContainer,
			EnergySourceContainer sourceContainer) const;

                /**
                  * Date to create the file
                  */
                  void SetDate (std::string date); ///ggarciabas

	private:
		/**
		* \param device Pointer to the NetDevice to install DeviceEnergyModel.
		*
		* Implements DeviceEnergyModel::Install.
		* This function is replaced by LteInstall.
		*/
		virtual Ptr<DeviceEnergyModel> DoInstall (Ptr<NetDevice> device,
			Ptr<EnergySource> source) const;

		ObjectFactory m_radioEnergy;
		LteRadioEnergyModel::LteRadioEnergyDepletionCallback m_depletionCallback;
		Ptr<LteRadioEnergyStatsCalculator> m_pLteRadioEnergyStatsCalculator;
	};
}

#endif
